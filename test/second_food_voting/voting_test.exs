defmodule SecondFoodVoting.VotingTest do
  use ExUnit.Case

  alias SecondFoodVoting.Voting.Group
  alias SecondFoodVoting.Voting.GroupState
  alias SecondFoodVoting.Voting.Voter

  describe "Group.start_link/1" do
    test "accepts and id and owner on start" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      assert {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert %GroupState{owner: ^owner, id: ^id} = Group.get_state(pid)
    end
  end

  describe "Group.find_me/2" do
    test "owner is automatically a member" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      assert {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert owner == Group.find_me(pid, 1)
    end

    test "user is not member before joining" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      assert {:ok, pid} = Group.start_link(id: id, owner: owner)
      refute Group.find_me(pid, user.user_id)
    end

    test "user is member after joining" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      assert {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.join(pid, user)
      assert Group.find_me(pid, 2)
    end
  end

  describe "Simple case" do
    test "simple" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      test_truck = "Test Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.begin_voting(pid)
      assert :ok = Group.vote(pid, owner, test_truck)
      assert %GroupState{winner: ^test_truck, phase: :finished} = Group.get_state(pid)
    end
  end

  describe "Multiple Voters" do
    test "two" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      test_truck = "Test Truck"
      more_truck = "More Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.join(pid, user)
      assert :ok = Group.nominate(pid, user, more_truck)
      assert :ok = Group.begin_voting(pid)
      assert :ok = Group.vote(pid, owner, test_truck)
      assert :ok = Group.vote(pid, user, test_truck)
      assert %GroupState{winner: ^test_truck, phase: :finished} = Group.get_state(pid)
    end

    test "three" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      user_3 = %Voter{user_id: 3, name: "User 3"}
      id = "testing-123"
      test_truck = "Test Truck"
      more_truck = "More Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.join(pid, user)
      assert :ok = Group.join(pid, user_3)
      assert :ok = Group.nominate(pid, user, more_truck)
      assert :ok = Group.begin_voting(pid)
      assert :ok = Group.vote(pid, owner, test_truck)
      assert :ok = Group.vote(pid, user, more_truck)
      assert :ok = Group.vote(pid, user_3, more_truck)
      assert %GroupState{winner: ^more_truck, phase: :finished} = Group.get_state(pid)
    end
  end

  describe "Nominations" do
    test "nominating after voting has begun" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      test_truck = "Test Truck"
      more_truck = "More Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.begin_voting(pid)
      assert {:error, :not_nominating} = Group.nominate(pid, owner, more_truck)
      expected = MapSet.new([{owner, test_truck}])
      assert %GroupState{nominations: ^expected} = Group.get_state(pid)
    end

    test "nomination by non-member" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      test_truck = "Test Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert {:error, :not_member} = Group.nominate(pid, user, test_truck)
      expected = MapSet.new()
      assert %GroupState{nominations: ^expected} = Group.get_state(pid)
    end

    test "multiple nominations by same voter" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      test_truck = "Test Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.nominate(pid, owner, test_truck)
      expected = MapSet.new([{owner, test_truck}])
      assert %GroupState{nominations: ^expected} = Group.get_state(pid)
    end

    test "multiple nominations by different voters" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      test_truck = "Test Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.join(pid, user)
      assert :ok = Group.nominate(pid, user, test_truck)
      expected = MapSet.new([{owner, test_truck}, {user, test_truck}])
      assert %GroupState{nominations: ^expected} = Group.get_state(pid)
    end
  end

  describe "Voting" do
    test "voting for truck not nominated" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      test_truck = "Test Truck"
      more_truck = "More Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.begin_voting(pid)
      assert {:error, :not_nominated} = Group.vote(pid, owner, more_truck)
      assert %GroupState{votes: [], phase: :voting} = Group.get_state(pid)
    end

    test "begin voting with no nominations should error" do
      owner = %Voter{user_id: 1, name: "Tester"}
      id = "testing-123"
      test_truck = "Test Truck"
      more_truck = "More Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert {:error, :no_nominations} = Group.begin_voting(pid)
      assert %GroupState{votes: [], phase: :nominating} = Group.get_state(pid)
    end

    test "voters can change their votes" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      test_truck = "Test Truck"
      more_truck = "More Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.join(pid, user)
      assert :ok = Group.nominate(pid, user, more_truck)
      assert :ok = Group.begin_voting(pid)
      assert :ok = Group.vote(pid, owner, test_truck)
      assert :ok = Group.vote(pid, owner, more_truck)
      assert %GroupState{votes: [{^owner, ^more_truck}], phase: :voting} = Group.get_state(pid)
    end
  end

  describe "Reset" do
    test "should save voters" do
      owner = %Voter{user_id: 1, name: "Tester"}
      user = %Voter{user_id: 2, name: "User"}
      id = "testing-123"
      test_truck = "Test Truck"
      {:ok, pid} = Group.start_link(id: id, owner: owner)
      assert :ok = Group.nominate(pid, owner, test_truck)
      assert :ok = Group.join(pid, user)
      assert :ok = Group.reset(pid)
      expected = MapSet.new([owner, user])
      assert %GroupState{nominations: %MapSet{}, voters: ^expected} = Group.get_state(pid)
    end
  end
end
