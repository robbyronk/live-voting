alias SecondFoodVoting.Voting.Group
alias SecondFoodVoting.Voting.Voter

import_if_available(Ecto.Query)
import_if_available(Ecto.Changeset)

defmodule App do
  @moduledoc false
  def restart do
    Application.stop(:second_food_voting)
    recompile()
    Application.ensure_all_started(:second_food_voting)
  end
end
