defmodule SecondFoodVoting.Repo do
  use Ecto.Repo,
    otp_app: :second_food_voting,
    adapter: Ecto.Adapters.SQLite3
end
