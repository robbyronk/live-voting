defmodule SecondFoodVoting.Mailer do
  @moduledoc false
  use Swoosh.Mailer, otp_app: :second_food_voting
end
