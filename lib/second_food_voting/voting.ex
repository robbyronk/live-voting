defmodule SecondFoodVoting.Voting do
  @moduledoc false
  alias SecondFoodVoting.Voting.Group
  alias SecondFoodVoting.Voting.Voter

  @registry SecondFoodVoting.VotingGroups.Registry
  @supervisor SecondFoodVoting.VotingGroups.Supervisor

  def start(id, %Voter{} = owner) do
    opts = [
      id: id,
      owner: owner,
      name: {:via, Registry, {@registry, id}}
    ]

    DynamicSupervisor.start_child(@supervisor, {Group, opts})
  end

  def lookup(id) do
    case Registry.lookup(@registry, id) do
      [{pid, _}] -> {:ok, pid}
      [] -> {:error, :not_found}
    end
  end

  def join(id, voter) do
    with {:ok, pid} <- lookup(id) do
      Group.join(pid, voter)
    end
  end

  def get_state(id) when is_binary(id) do
    with {:ok, pid} <- lookup(id) do
      Group.get_state(pid)
    end
  end

  def find_me(id, user_id) do
    with {:ok, pid} <- lookup(id) do
      Group.find_me(pid, user_id)
    end
  end

  def nominate(id, voter, nomination) do
    with {:ok, pid} <- lookup(id) do
      Group.nominate(pid, voter, nomination)
    end
  end

  def vote(id, voter, nomination) do
    with {:ok, pid} <- lookup(id) do
      Group.vote(pid, voter, nomination)
    end
  end

  def begin_voting(id) do
    with {:ok, pid} <- lookup(id) do
      Group.begin_voting(pid)
    end
  end

  def reset(id) do
    with {:ok, pid} <- lookup(id) do
      Group.reset(pid)
    end
  end
end
