defmodule SecondFoodVoting.Voting.GroupState do
  @moduledoc false
  defstruct [
    :id,
    :owner,
    :phase,
    :nominations,
    :trucks,
    :votes,
    :voters,
    :winner
  ]

  @doc """
  Keeping it simple here and loading a local file with the options.
  """
  def read_food_truck_names do
    csv_rows =
      "Mobile_Food_Facility_Permit.csv"
      |> File.stream!([:trim_bom])
      |> CSV.decode!(headers: true, field_transform: &String.trim/1)
      |> Enum.into([])

    for_result =
      for %{"Applicant" => applicant} <- csv_rows, is_binary(applicant) and applicant != "" do
        applicant
      end

    MapSet.new(for_result)
  end

  def initial_state(id, owner) do
    %__MODULE__{
      id: id,
      owner: owner,
      phase: :nominating,
      nominations: MapSet.new(),
      trucks: read_food_truck_names(),
      votes: [],
      voters: MapSet.new([owner]),
      winner: nil
    }
  end
end
