defmodule SecondFoodVoting.Voting.Voter do
  @moduledoc false
  defstruct [:user_id, :name]

  def new(user_id, name) do
    %__MODULE__{user_id: user_id, name: name}
  end
end
