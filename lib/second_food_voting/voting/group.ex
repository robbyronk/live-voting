defmodule SecondFoodVoting.Voting.Group do
  @moduledoc """
  VotingGroup is a GenServer that holds the state of a voting group and exposes an API to nominate, vote and see the state.
    

  """

  use GenServer, restart: :transient

  alias SecondFoodVoting.Voting.GroupState
  alias SecondFoodVoting.Voting.Voter

  require Logger

  @timeout :timer.minutes(30)

  def find_me(pid, user_id) when is_pid(pid) and is_integer(user_id) do
    GenServer.call(pid, {:find_me, user_id})
  end

  def join(pid, %Voter{} = voter) when is_pid(pid) do
    GenServer.call(pid, {:join, voter})
  end

  def get_state(pid) when is_pid(pid) do
    GenServer.call(pid, :get_state)
  end

  def nominate(pid, %Voter{} = voter, nomination) when is_pid(pid) and is_binary(nomination) do
    GenServer.call(pid, {:nominate, voter, nomination})
  end

  def begin_voting(pid) when is_pid(pid) do
    GenServer.call(pid, :begin_voting)
  end

  def vote(pid, %Voter{} = voter, vote) when is_pid(pid) and is_binary(vote) do
    GenServer.call(pid, {:vote, voter, vote})
  end

  def reset(pid) do
    GenServer.call(pid, :reset)
  end

  defp reply_and_broadcast(response, state) do
    Phoenix.PubSub.broadcast(SecondFoodVoting.PubSub, "group:#{state.id}", state)
    {:reply, response, state, @timeout}
  end

  defp reply(response, state) do
    {:reply, response, state, @timeout}
  end

  def start_link(opts) do
    {name, opts} = Keyword.pop(opts, :name)
    GenServer.start_link(__MODULE__, opts, name: name)
  end

  @impl true
  def init(id: id, owner: owner) do
    state = GroupState.initial_state(id, owner)

    {:ok, state, @timeout}
  end

  @impl true
  def handle_info(:timeout, state) do
    Logger.info("shutting down #{state.id}")

    {:stop, :normal, state}
  end

  @impl GenServer
  def handle_call({:find_me, user_id}, _from, state) do
    state.voters
    |> Enum.find(fn %Voter{user_id: i} -> i == user_id end)
    |> reply(state)
  end

  @impl true
  def handle_call({:join, voter}, _from, state) do
    reply(:ok, %{state | voters: MapSet.put(state.voters, voter)})
  end

  @impl GenServer
  def handle_call(:get_state, _from, state) do
    reply(state, state)
  end

  @impl GenServer
  def handle_call({:nominate, voter, nomination}, _from, state) do
    cond do
      state.phase != :nominating ->
        reply({:error, :not_nominating}, state)

      !MapSet.member?(state.voters, voter) ->
        reply({:error, :not_member}, state)

      true ->
        reply_and_broadcast(:ok, %{
          state
          | nominations: MapSet.put(state.nominations, {voter, nomination})
        })
    end
  end

  @impl GenServer
  def handle_call(:begin_voting, _from, state) do
    if MapSet.size(state.nominations) > 0 do
      reply_and_broadcast(:ok, %{state | phase: :voting})
    else
      reply({:error, :no_nominations}, state)
    end
  end

  @impl GenServer
  def handle_call({:vote, voter, vote}, _from, state) do
    if is_nominated?(state, vote) do
      votes = Enum.reject(state.votes, fn {v, _} -> v == voter end)
      next_state = %{state | votes: [{voter, vote} | votes]}

      reply_and_broadcast(:ok, maybe_finish(next_state))
    else
      reply({:error, :not_nominated}, state)
    end
  end

  @impl GenServer
  def handle_call(:reset, _from, state) do
    clean_state = GroupState.initial_state(state.id, state.owner)
    reply_and_broadcast(:ok, %{clean_state | voters: state.voters})
  end

  defp maybe_finish(state) do
    if all_voted?(state) do
      %{state | phase: :finished, winner: find_winner(state)}
    else
      state
    end
  end

  defp is_nominated?(state, vote) when is_binary(vote) do
    state.nominations
    |> Enum.map(fn {_voter, nomination} -> nomination end)
    |> MapSet.new()
    |> MapSet.member?(vote)
  end

  defp all_voted?(state) do
    length(state.votes) == MapSet.size(state.voters)
  end

  defp find_winner(state) do
    # todo choose random winner with max votes
    state.votes
    |> Enum.map(fn {_voter, vote} -> vote end)
    |> Enum.frequencies()
    |> Enum.max_by(fn {_vote, count} -> count end)
    |> elem(0)
  end
end
