defmodule SecondFoodVotingWeb.PageHTML do
  use SecondFoodVotingWeb, :html

  embed_templates "page_html/*"
end
