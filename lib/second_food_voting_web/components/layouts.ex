defmodule SecondFoodVotingWeb.Layouts do
  @moduledoc false
  use SecondFoodVotingWeb, :html

  embed_templates "layouts/*"
end
