defmodule SecondFoodVotingWeb.Nominations do
  @moduledoc false
  #  use SecondFoodVotingWeb, :live_component

  use Phoenix.Component

  def nominations_list(assigns) do
    ~H"""
    nominations
    """
  end
end
