defmodule SecondFoodVotingWeb.VotingGroupComponents do
  @moduledoc """
  Provides UI components for Voting Groups.
  """

  use Phoenix.Component

  import SecondFoodVotingWeb.CoreComponents

  alias SecondFoodVoting.Voting.GroupState

  @doc """
  Renders the header for the Voting Group.
  """
  attr :state, GroupState, default: nil

  def group_header(assigns) do
    ~H"""
    <.header class="text-center"><%= @state.owner.name %>'s Lunch Vote</.header>
    """
  end

  @doc """
  Renders a simple list of voters.
  """
  attr :state, GroupState, default: nil

  def voter_list(assigns) do
    ~H"""
    <h2 class="font-bold text-brand">Voters in the group:</h2>
    <ul class="list-disc">
      <li :for={v <- @state.voters}><%= v.name %></li>
    </ul>
    """
  end

  def nomination_form(assigns) do
    ~H"""
    <.simple_form for={%{}} phx-submit="nominate_truck">
      <.input
        type="select"
        id="truck"
        name="truck"
        errors={[]}
        label="Truck"
        options={@truck_options}
        value={nil}
      />
      <:actions>
        <.button phx-disable-with="Nominating...">Nominate</.button>
      </:actions>
    </.simple_form>
    """
  end

  def name_form(assigns) do
    ~H"""
    <.simple_form
      :let={f}
      for={@join_form}
      as={:join_form}
      phx-change="validate_name"
      phx-submit="join_group"
    >
      <.input field={f[:name]} label="Your First Name" />

      <:actions>
        <.button>Join Group</.button>
      </:actions>
    </.simple_form>
    """
  end
end
