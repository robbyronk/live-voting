defmodule SecondFoodVotingWeb.CreateGroupLive do
  @moduledoc false
  use SecondFoodVotingWeb, :live_view

  alias SecondFoodVoting.Accounts
  alias SecondFoodVoting.Accounts.User
  alias SecondFoodVoting.Voting
  alias SecondFoodVoting.Voting.Voter

  @create_form_types %{name: :string}

  @initial_assigns %{
    create_form: Ecto.Changeset.change({%{name: ""}, @create_form_types}),
    user_id: nil
  }

  @impl true
  def render(assigns) do
    ~H"""
    <div class="px-4 py-10 sm:px-6 sm:py-28 lg:px-8 xl:px-28 xl:py-32">
      <div class="mx-auto max-w-xl lg:mx-0">
        <h1 class="text-brand mt-10 flex items-center text-sm font-semibold leading-6">
          Friends Fairly Fooding
        </h1>
        <p class="text-[2rem] mt-4 font-semibold leading-10 tracking-tighter text-orange">
          Skip the debate, vote, and go eat.
        </p>
        <p class="mt-4 text-base leading-7 text-orange">
          Create a group and invite your friends to vote on what food truck to go to.
        </p>
        <div class="flex">
          <div class="w-full sm:w-auto">
            <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-4 sm:grid-cols-3">
              <.simple_form
                :let={f}
                for={@create_form}
                as={:create_form}
                phx-change="validate"
                phx-submit="create"
              >
                <.input field={f[:name]} label="Your First Name" />

                <:actions>
                  <.button>Create Group</.button>
                </:actions>
              </.simple_form>
            </div>
          </div>
        </div>
      </div>
    </div>
    """
  end

  def assign_user_id(socket, %{"user_token" => token}) when is_binary(token) do
    %User{id: user_id} = Accounts.get_user_by_session_token(token)
    assign(socket, user_id: user_id)
  end

  @impl true
  def mount(_params, session, socket) do
    {
      :ok,
      socket
      |> assign(@initial_assigns)
      |> assign_user_id(session)
    }
  end

  @impl true
  def handle_event("validate", %{"create_form" => form_params}, socket) do
    changeset =
      {%{}, @create_form_types}
      |> Ecto.Changeset.cast(form_params, Map.keys(@create_form_types))
      |> Ecto.Changeset.validate_required(Map.keys(@create_form_types))

    if changeset.valid? do
    else
    end

    {:noreply, assign(socket, create_form: changeset)}
  end

  @impl true
  def handle_event("create", %{"create_form" => form_params}, socket) do
    changeset =
      {%{}, @create_form_types}
      |> Ecto.Changeset.cast(form_params, Map.keys(@create_form_types))
      |> Ecto.Changeset.validate_required(Map.keys(@create_form_types))

    case Ecto.Changeset.apply_action(changeset, :update) do
      {:ok, %{name: name}} ->
        id = Ecto.UUID.generate()
        {:ok, _pid} = Voting.start(id, Voter.new(socket.assigns.user_id, name))
        {:noreply, push_navigate(socket, to: ~p"/groups/#{id}")}

      {:error, changeset} ->
        {:noreply, assign(socket, create_form: changeset)}
    end
  end
end
