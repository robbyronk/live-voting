defmodule SecondFoodVotingWeb.ViewGroupLive do
  @moduledoc false
  use SecondFoodVotingWeb, :live_view

  import SecondFoodVotingWeb.VotingGroupComponents

  alias SecondFoodVoting.Voting
  alias SecondFoodVoting.Voting.GroupState
  alias SecondFoodVoting.Voting.Voter

  @join_form_types %{name: :string}

  @initial_assigns %{
    group_state: :loading,
    voter: nil,
    join_form: Ecto.Changeset.change({%{name: ""}, @join_form_types}),
    truck_options: [],
    all_truck_options: []
  }

  @impl true
  def render(%{voter: nil} = assigns) do
    ~H"""
    <div class="mx-auto max-w-sm">
      <.group_header state={@group_state} />
      <div class="grid grid-cols-2 gap-4">
        <div>
          <.voter_list state={@group_state} />
        </div>
        <div>
          <.name_form join_form={@join_form} />
        </div>
      </div>
    </div>
    """
  end

  def render(%{group_state: %{phase: :nominating}} = assigns) do
    ~H"""
    <div class="mx-auto max-w-md">
      <.group_header state={@group_state} />
      <div class="grid grid-cols-2 gap-4">
        <div>
          <.voter_list state={@group_state} />
          <div :if={@group_state.owner.user_id == @current_user.id} class="mt-4">
            <.button phx-click="begin_voting">Begin voting</.button>
          </div>
        </div>
        <div>
          <.nomination_form truck_options={@truck_options} />
          <ul>
            <li :for={n <- @group_state.nominations}><%= elem(n, 1) %></li>
          </ul>
        </div>
      </div>
    </div>
    """
  end

  def render(%{group_state: %{phase: :voting}} = assigns) do
    ~H"""
    <div class="mx-auto max-w-sm">
      <.group_header state={@group_state} />
      <div class="grid grid-cols-2 gap-4">
        <div>
          <.voter_list state={@group_state} />
        </div>
        <div>
          <div :for={n <- @group_state.nominations}>
            <.button phx-click="vote" phx-value-truck={elem(n, 1)}>
              <%= elem(n, 1) %>
            </.button>
          </div>
        </div>
      </div>
    </div>
    """
  end

  def render(%{group_state: %{phase: :finished}} = assigns) do
    ~H"""
    <div class="mx-auto max-w-sm">
      <.group_header state={@group_state} />
      <div :if={@group_state.owner.user_id == @current_user.id}>
        <.button phx-click="reset">Reset</.button>
      </div>
      The winner is: <%= @group_state.winner %>
    </div>
    """
  end

  defp assign_latest_group_state(socket) do
    case Voting.get_state(socket.assigns.group_id) do
      {:error, :not_found} ->
        push_navigate(socket, to: ~p"/groups/create")

      %GroupState{} = group_state ->
        assign(socket, group_state: group_state)
    end
  end

  defp get_me(socket) do
    voter = Voting.find_me(socket.assigns.group_id, socket.assigns.current_user.id)
    assign(socket, voter: voter)
  end

  defp assign_truck_options(socket) do
    truck_options = Enum.map(socket.assigns.group_state.trucks, fn name -> {name, name} end)

    assign(socket, truck_options: truck_options, all_truck_options: truck_options)
  end

  @impl true
  def mount(%{"id" => group_id}, _session, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(SecondFoodVoting.PubSub, "group:#{group_id}")
    end

    {
      :ok,
      socket
      |> assign(@initial_assigns)
      |> assign(group_id: group_id)
      |> assign_latest_group_state()
      |> assign_truck_options()
      |> get_me()
    }
  end

  @impl true
  def handle_info(%GroupState{} = group_state, socket) do
    nominated_trucks = Enum.map(group_state.nominations, fn {_voter, truck} -> truck end)

    truck_options =
      Enum.reject(socket.assigns.all_truck_options, fn {truck, _} -> Enum.member?(nominated_trucks, truck) end)

    {
      :noreply,
      assign(
        socket,
        group_state: group_state,
        truck_options: truck_options
      )
    }
  end

  @impl true
  def handle_event("validate_name", %{"join_form" => form_params}, socket) do
    changeset =
      {%{}, @join_form_types}
      |> Ecto.Changeset.cast(form_params, Map.keys(@join_form_types))
      |> Ecto.Changeset.validate_required(Map.keys(@join_form_types))

    {:noreply, assign(socket, join_form: changeset)}
  end

  @impl true
  def handle_event("join_group", %{"join_form" => form_params}, socket) do
    changeset =
      {%{}, @join_form_types}
      |> Ecto.Changeset.cast(form_params, Map.keys(@join_form_types))
      |> Ecto.Changeset.validate_required(Map.keys(@join_form_types))

    case Ecto.Changeset.apply_action(changeset, :update) do
      {:ok, %{name: name}} ->
        new_voter = Voter.new(socket.assigns.current_user.id, name)
        Voting.join(socket.assigns.group_id, new_voter)
        {:noreply, assign(socket, voter: new_voter)}

      {:error, changeset} ->
        {:noreply, assign(socket, join_form: changeset)}
    end
  end

  @impl true
  def handle_event("nominate_truck", %{"truck" => truck_name}, socket) do
    Voting.nominate(socket.assigns.group_id, socket.assigns.voter, truck_name)

    {:noreply, socket}
  end

  @impl true
  def handle_event("begin_voting", _params, socket) do
    Voting.begin_voting(socket.assigns.group_id)

    {:noreply, socket}
  end

  @impl true
  def handle_event("vote", %{"truck" => truck_name}, socket) do
    Voting.vote(socket.assigns.group_id, socket.assigns.voter, truck_name)

    {:noreply, socket}
  end

  @impl true
  def handle_event("reset", _params, socket) do
    Voting.reset(socket.assigns.group_id)

    {:noreply, socket}
  end
end
